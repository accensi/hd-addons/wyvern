### Important
---
- This mod requires [BulletLib](https://gitlab.com/accensi/hd-addons/hdbulletlib).

### Notes
---
- The loadout code is `wyv`.
- Configuration codes are:
	- `auto`: Autoloader. Makes reloading from side saddles a bit faster.